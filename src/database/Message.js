const mongoose = require('mongoose');

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const messageSchema = new mongoose.Schema({
    email: {
        type: mongoose.SchemaTypes.String,
        required: true,
        validate: {
            validator: (value) => {
                return EMAIL_REGEX.test(value.toLowerCase());
            },
            message: (props) => `${props.value} is invalid!`
        }
    },
    text: {
        type: mongoose.SchemaTypes.String,
        required: true,
        minlength: 1,
        maxlength: 100
    },
    createdAt: {
        type: mongoose.SchemaTypes.Date,
        default: new Date()
    },
    modifiedAt: {
        type: mongoose.SchemaTypes.Date,
        default: new Date()
    }
});

module.exports = mongoose.model('Message', messageSchema, 'messages');
