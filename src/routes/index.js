const Router = require('koa-router');
const apiRouter = require('./api');

const router = Router();

router.use('/api', apiRouter.routes());
router.use('/api', apiRouter.allowedMethods());

module.exports = router;
