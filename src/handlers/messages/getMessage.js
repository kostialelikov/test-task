const JoiBase = require('joi');
const ObjectId = require('joi-mongodb-objectid');

const Joi = JoiBase.extend(ObjectId);

const getMessage = async (ctx) => {
    const id = ctx.params.id;
    try {
        await Joi.objectId().validate(id);
    } catch (e) {
        ctx.throw(400, e.message)
    }
    const message = await ctx.db.Message.findById(id);
    if (message)
        ctx.body = {
            message
        };
    else
        ctx.throw(404, 'Message not found.');
};

module.exports= getMessage;
