const request = require('supertest');
const {pick} = require('lodash');
const JoiBase = require('joi');
const ObjectId = require('joi-mongodb-objectid');
const mongoose = require("mongoose");
const {app, server} = require('../src');


const Joi = JoiBase.extend(ObjectId);

describe('getting and posting messages', () => {
    const IDs = [];
    beforeAll(async () => {
        for (let i = 0; i < 3; i++) {
            const user = await app.context.db.Message.create({
                email: 'test@test.com',
                text: 'test' + i
            });
            IDs.push(user._id);
        }
    });
    afterAll(async done => {
       await mongoose.connection.close();
       await server.close();
       done();
    });
    it('should have 3 entities', async () => {
        const response = await request(app.callback()).get('/api/messages/list/0');
        expect(response.status).toEqual(200);
        expect(response.body.messages.length).toEqual(3);
    });
    it('should return message with test: "test0"', async () => {
        const response = await request(app.callback()).get(`/api/messages/single/${IDs[0]}`);
        expect(response.status).toEqual(200);
        expect(response.body.message.text).toEqual('test0');
    });
    it('should post new message', async () => {
        const message = {
            email: 'test@test.com',
            text: 'test'
        };
        const response = await request(app.callback()).post('/api/messages').send(pick(message, ['email', 'text']));
        expect(response.status).toEqual(200);
        await Joi.objectId().validate(response.body.id);
    });
});
