const koa = require('koa');
const bodyParser = require('koa-bodyparser');
const db = require('./database');
const router = require('./routes');
const config = require('../config');


const app = new koa();
app.use(bodyParser({
        enableTypes: ['json'],
        jsonLimit: '1mb'
    }));
app.use(router.routes());
app.use(router.allowedMethods());
app.context.db = db;
app.context.config = config;

const server = app.listen(config.app.port, () => {
   console.error(`Server listening at port: ${config.app.port}`);
});

module.exports = {
    server,
    app
};
