const Router = require('koa-router');
const validate = require('koa-joi-validate');
const getMessages = require('../handlers/messages/getMessages');
const getMessage = require('../handlers/messages/getMessage');
const postMessage = require('../handlers/messages/postMessage');

const router = Router();

router.get('/list/:page',  getMessages);
router.get('/single/:id', getMessage);
router.post('/', validate(postMessage.schema), postMessage);

module.exports = router;
