const Joi = require("joi");

module.exports = Joi.object({
    env: Joi.string()
        .valid(['production', 'development', 'test'])
        .default(process.env.NODE_ENV || 'development')
        .optional(),
    mongoose: Joi.object({
        host: Joi.string(),
        port: Joi.number(),
        db: Joi.string(),
        user: Joi.string().optional().default(''),
        password: Joi.string().optional().default('')
    }),
    app: Joi.object({
        port: Joi.number(),
        host: Joi.string().default('localhost').optional()
    })
});
