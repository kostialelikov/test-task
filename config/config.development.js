module.exports = {
    mongoose: {
        host: 'localhost',
        port: 27017,
        db: 'test-task'
    },
    app: {
        port: 3000
    }
};
