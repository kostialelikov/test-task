const Router = require('koa-router');
const messagesRouter = require('./messages');

const router = Router();

router.use('/messages', messagesRouter.routes());
router.use('/messages', messagesRouter.allowedMethods());

module.exports = router;
