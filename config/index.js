const configLoader = require('../src/helpers/configLoader');
const schema = require('./config.schema');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

module.exports = configLoader(__dirname, schema);
