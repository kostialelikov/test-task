const PER_PAGE = 10;

const getMessages = async (ctx) => {
    const page = ctx.params.page;
    if (page < 0) ctx.throw(400, 'Invalid page number.');
    const count = await ctx.db.Message.count();
    const pages = Math.ceil(count / PER_PAGE);
    if (page >= pages && page !== 0) ctx.throw(400, 'Invalid page number.');
    const messages = await ctx.db.Message.find().limit(PER_PAGE).skip(PER_PAGE * page).sort('-createdAt');
    ctx.body = {
        messages,
        page,
        pages
    }
};

module.exports = getMessages;
