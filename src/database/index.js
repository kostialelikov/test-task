const mongoose = require('mongoose');
const config = require('../../config');
const message = require('./Message');

mongoose.Promise = global.Promise;

(async () => {
    const mongo_url = `mongodb://${config.mongoose.user}:${config.mongoose.password}@${config.mongoose.host}:${config.mongoose.port}/${config.mongoose.db}?socketTimeoutMS=120000`;
    let db;
    if (process.env.NODE_ENV === 'test' && !process.env.CI) {
        const Mockgoose = require('mockgoose').Mockgoose;
        const mockgoose = new Mockgoose(mongoose);
        await mockgoose.prepareStorage();
        db = await mongoose.connect(mongo_url);
    } else {
        db = await mongoose
            .connect(mongo_url, {useNewUrlParser: true});
    }
    process.on('SIGINT', () => {
        console.log('Disconnecting database');
        db.disconnect();
        console.log('Database disconnected');
    });
})();

module.exports = {
    Message: message
};
