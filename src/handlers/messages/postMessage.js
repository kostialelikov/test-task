const Joi = require('joi');

const postMessage = async (ctx) => {
    const {email, text} = ctx.request.body;
    try {
        const message =  await ctx.db.Message.create({
            email,
            text
        });
        ctx.body = {
            id: message._id
        };
    } catch (e) {
        ctx.throw(400, e.message)
    }
};

const TEXT_PATTERN = /^.{1,150}$/;

postMessage.schema = {
    body: {
        email: Joi.string().email(),
        text: Joi.string().regex(TEXT_PATTERN)
    }
};

module.exports = postMessage;
